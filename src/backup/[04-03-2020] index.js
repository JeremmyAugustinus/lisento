import React from 'react';
import ReactDOM from 'react-dom';
import { Bar } from 'react-chartjs-2';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';
import './index.scss';

firebase.initializeApp({
    apiKey: "AIzaSyB6h_swNbJvCh-e_ZF-4k8fOqfhjFbD6Rg",
    authDomain: "ithb-social-media-analytics.firebaseapp.com",
    databaseURL: "https://ithb-social-media-analytics.firebaseio.com",
    projectId: "ithb-social-media-analytics",
    storageBucket: "ithb-social-media-analytics.appspot.com",
    messagingSenderId: "1066715358144",
    appId: "1:1066715358144:web:ad045d76446dd2ea96de25"
});

class Graph extends React.Component {
    render() {
        return (
            <div class="graph">
                <div class="card-header bg-transparent">
                    <ul class="nav card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#minggu" data-toggle="tab">MINGGU</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#bulan" data-toggle="tab">BULAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tahun" data-toggle="tab">TAHUN</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body tab-content">
                    <div id="minggu" class="tab-pane fade show active">
                        <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(20, 143, 30, 1)',
                                        borderColor: 'rgba(20, 143, 30, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(176, 24, 21, 1)',
                                        borderColor: 'rgba(176, 24, 21, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="bulan" class="tab-pane fade" role="tabpanel">
                        <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(20, 143, 30, 1)',
                                        borderColor: 'rgba(20, 143, 30, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(176, 24, 21, 1)',
                                        borderColor: 'rgba(176, 24, 21, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="tahun" class="tab-pane fade" role="tabpanel">
                        <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(20, 143, 30, 1)',
                                        borderColor: 'rgba(20, 143, 30, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(176, 24, 21, 1)',
                                        borderColor: 'rgba(176, 24, 21, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            labels: [],
            data: {
                total: [],
                positif: [],
                negatif: []
            }
        };
    }
    componentDidMount() {
        firebase.database().ref("/").orderByChild("datetime").on("value", snapshot => {
            let today, tomorrow = null;
            let temp_lab = [];
            let temp_total = [];
            let temp_positif = [];
            let temp_negatif = [];
            let temp_tweets = [];
            snapshot.forEach(data => {
                temp_tweets.push(
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <b>{data.val().user} - {data.val().datetime} </b>
                                    {data.val().data}
                                </div>
                            </div>
                        </div>
                    </div>
                )
                today = new Date(data.val().datetime.split(/\//).reverse().join('/')); // last date object to date
                tomorrow = [ (today.getDate()+1), ("0" + (today.getMonth() + 1)).slice(-2), today.getFullYear() ].join('/'); // day++ (tomorrow)
                if (temp_lab.length === 0) {
                    temp_lab.push(data.val().datetime);
                    temp_total.push(1);
                    if (data.val().sentiment === "positif") {
                        temp_positif.push(1);
                        temp_negatif.push(0);
                    } else if (data.val().sentiment === "negatif") {
                        temp_positif.push(0);
                        temp_negatif.push(1);
                    }
                    temp_lab.push(tomorrow);
                    temp_total.push(0);
                    temp_positif.push(0);
                    temp_negatif.push(0);
                } else {
                    if (data.val().datetime === temp_lab[temp_lab.length-1] && data.val().sentiment !== "") {
                        temp_total[temp_total.length-1] += 1;
                        if (data.val().sentiment === "positif") {
                            temp_positif[temp_positif.length-1] += 1;
                        } else if (data.val().sentiment === "negatif") {
                            temp_negatif[temp_negatif.length-1] += 1;
                        }
                    } else {
                        while (data.val().datetime !== temp_lab[temp_lab.length-1]) {
                            today = new Date(temp_lab[temp_lab.length-1].split(/\//).reverse().join('/')); // last date object to date
                            tomorrow = [ (today.getDate()+1), ("0" + (today.getMonth() + 1)).slice(-2), today.getFullYear() ].join('/'); // day++ (tomorrow)
                            temp_lab.push(tomorrow);
                            temp_total.push(0);
                            temp_positif.push(0);
                            temp_negatif.push(0);
                            if (data.val().datetime === temp_lab[temp_lab.length-1] && data.val().sentiment !== "") {
                                temp_total[temp_total.length-1] += 1;
                                if (data.val().sentiment === "positif") {
                                    temp_positif[temp_positif.length-1] += 1;
                                } else if (data.val().sentiment === "negatif") {
                                    temp_negatif[temp_negatif.length-1] += 1;
                                }
                            }                        
                        }
                    }
                }
            });
            if (temp_total[temp_total.length -1] === 0) {
                temp_lab.pop();
                temp_total.pop();
                temp_positif.pop();
                temp_negatif.pop();
            }
            this.setState({
                tweets:temp_tweets,
                labels:temp_lab,
                data:{
                    total:temp_total,
                    positif:temp_positif,
                    negatif:temp_negatif
                }
            });
        });
    }
    render() {
        return (
            <div className="dashboard">
                <div className="sidebar">
                    <img class="logo-expand" src="ithb-wh2.png" alt="logo" />
                    <ul>
                        <li><i class="fa fa-area-chart"></i><span>Dashboard</span></li>
                        <li><i class="fa fa-gear"></i><span>Setting</span></li>
                        <li><i class="fa fa-power-off"></i><span>Logout</span></li>
                    </ul>
                </div>
                <div className="content">
                    <div class="container-fluid">
                        <div class="row h-100">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <Graph 
                                            label_props={this.state.labels}
                                            total_props={this.state.data.total}
                                            positif_props={this.state.data.positif}
                                            negatif_props={this.state.data.negatif}
                                        />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4" style={{backgroundColor: '#148f1e'}}>
                                        <div class="card-body">
                                            <h2><b>{this.state.data.positif.reduce(function(a,b){ return a + b }, 0)}</b></h2>
                                            <i>total tweet positif</i>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" style={{backgroundColor: '#b01815'}}>
                                        <div class="card-body">
                                            <h2><b>{this.state.data.negatif.reduce(function(a,b){ return a + b }, 0)}</b></h2>
                                            <i>total tweet negatif</i>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 bg-warning">
                                        <div class="card-body">
                                            <h2><b>{this.state.data.total.reduce(function(a,b){ return a + b }, 0)}</b></h2>
                                            <i>total sentimen</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 feed-container">
                                <div class="feed">
                                    {this.state.tweets}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Dashboard />, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
