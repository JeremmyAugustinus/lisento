import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';
import { Bar } from 'react-chartjs-2';

firebase.initializeApp({
    apiKey: "AIzaSyB6h_swNbJvCh-e_ZF-4k8fOqfhjFbD6Rg",
    authDomain: "ithb-social-media-analytics.firebaseapp.com",
    databaseURL: "https://ithb-social-media-analytics.firebaseio.com",
    projectId: "ithb-social-media-analytics",
    storageBucket: "ithb-social-media-analytics.appspot.com",
    messagingSenderId: "1066715358144",
    appId: "1:1066715358144:web:ad045d76446dd2ea96de25"
});

class Graph extends React.Component {
    render() {
        return (
            <div class="graph">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#minggu" data-toggle="tab">MINGGU</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#bulan" data-toggle="tab">BULAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tahun" data-toggle="tab">TAHUN</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body tab-content">
                    <div id="minggu" class="tab-pane fade show active">
                        <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TOTAL TWEET',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.total_props
                                    },
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(187, 225, 250, 1)',
                                        borderColor: 'rgba(187, 225, 250, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="bulan" class="tab-pane fade" role="tabpanel">
                    <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TOTAL TWEET',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.total_props
                                    },
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(187, 225, 250, 1)',
                                        borderColor: 'rgba(187, 225, 250, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="tahun" class="tab-pane fade" role="tabpanel">
                    <Bar 
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TOTAL TWEET',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.total_props
                                    },
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(187, 225, 250, 1)',
                                        borderColor: 'rgba(187, 225, 250, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes:[{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            labels: [],
            data: {
                total: [],
                positif: [],
                negatif: []
            }
        };
    }
    componentDidMount() {
        firebase.database().ref("/").orderByChild("datetime").on("value", snapshot => {
            let temp_lab = [];
            let temp_total = [];
            let temp_positif = [];
            let temp_negatif = [];
            let temp_tweets = [];
            let index = -1;
            snapshot.forEach(data => {
                temp_tweets.push(
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-2">

                                </div>
                                <div class="col-10">
                                    <b>{data.val().user} - {data.val().datetime} </b>
                                    {data.val().data}
                                </div>
                            </div>
                        </div>
                    </div>
                )
                if (data.val().sentiment === "") {
                    // do nothing
                } else {
                    if(temp_lab.indexOf(data.val().datetime) === -1) {
                        temp_lab.push(data.val().datetime);
                        temp_total.push(1);
                        if (data.val().sentiment === "positif") {
                            temp_positif.push(1);
                            temp_negatif.push(0);
                        } else if (data.val().sentiment === "negatif") {
                            temp_positif.push(0);
                            temp_negatif.push(1);
                        }
                        index += 1;
                    } else {
                        temp_total[index] += 1;
                        if (data.val().sentiment === "positif") {
                            temp_positif[index] += 1;
                        } else if (data.val().sentiment === "negatif") {
                            temp_negatif[index] += 1;
                        }
                    }
                }
            });
            this.setState({
                tweets:temp_tweets,
                labels:temp_lab,
                data:{
                    total:temp_total,
                    positif:temp_positif,
                    negatif:temp_negatif
                }
            });
        });
    }
    render() {
        return (
            <div className="dashboard">
                <div className="sidebar">
                    <nav>
                        <ul>
                            <li><div className="logo"></div></li>
                            <li><i class="fa fa-twitter"><b>TWITTER</b></i></li>
                            <li><i class="fa fa-youtube-square"><b>YOUTUBE</b></i></li>
                            <li><i class="fa fa-instagram"><b>INSTAGRAM</b></i></li>
                            <li><i class="fa fa-facebook"><b>FACEBOOK</b></i></li>
                            <li><i class="fa fa-gear"><b>SETTING</b></i></li>
                        </ul>
                    </nav>
                </div>
                <div className="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm nopadding">
                                <Graph 
                                    label_props={this.state.labels}
                                    total_props={this.state.data.total}
                                    positif_props={this.state.data.positif}
                                    negatif_props={this.state.data.negatif}
                                />
                            </div>
                            <div class="col-sm nopadding">
                                <div class="feed">
                                    {this.state.tweets}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Dashboard />, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
