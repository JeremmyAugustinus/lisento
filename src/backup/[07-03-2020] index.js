import React from 'react';
import ReactDOM from 'react-dom';
import { Bar } from 'react-chartjs-2';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';
import './index.scss';

firebase.initializeApp({
    apiKey: "AIzaSyB6h_swNbJvCh-e_ZF-4k8fOqfhjFbD6Rg",
    authDomain: "ithb-social-media-analytics.firebaseapp.com",
    databaseURL: "https://ithb-social-media-analytics.firebaseio.com",
    projectId: "ithb-social-media-analytics",
    storageBucket: "ithb-social-media-analytics.appspot.com",
    messagingSenderId: "1066715358144",
    appId: "1:1066715358144:web:ad045d76446dd2ea96de25"
});

function logout(){
    this.setState(state => ({
        isLoggedIn: !state.isLoggedIn
    }));
}

class Graph extends React.Component {
    render() {
        return (
            <div class="graph">
                <div class="card-header bg-transparent">
                    <ul class="nav card-header-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#minggu" data-toggle="tab">MINGGU</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#bulan" data-toggle="tab">BULAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tahun" data-toggle="tab">TAHUN</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body tab-content">
                    <div id="minggu" class="tab-pane fade show active">
                        <Bar
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="bulan" class="tab-pane fade" role="tabpanel">
                        <Bar
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                    <div id="tahun" class="tab-pane fade" role="tabpanel">
                        <Bar
                            data={{
                                labels: this.props.label_props,
                                datasets: [
                                    {
                                        label: 'TWEET POSITIF',
                                        backgroundColor: 'rgba(15, 76, 117, 1)',
                                        borderColor: 'rgba(15, 76, 117, 1)',
                                        data: this.props.positif_props
                                    },
                                    {
                                        label: 'TWEET NEGATIF',
                                        backgroundColor: 'rgba(50, 130, 184, 1)',
                                        borderColor: 'rgba(50, 130, 184, 1)',
                                        data: this.props.negatif_props
                                    }
                                ]
                            }}
                            options={{
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            labels: [],
            data: {
                total: [],
                positif: [],
                negatif: []
            }
        };
    }
    componentDidMount() {
        let val = "";
        firebase.database().ref("/").orderByChild("datetime").on("value", snapshot => {
            this.setState({
                tweets: [],
                labels: [],
                data: {
                    total: [],
                    positif: [],
                    negatif: []
                }
            });
            snapshot.forEach(data => {
                this.setState({
                    tweets: this.state.tweets.concat(
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <b>{data.val().user} - {data.val().datetime} </b>
                                        {data.val().data}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                });
                if (val === data.val().datetime) {
                    if (data.val().sentiment === "positif") {
                        this.state.data.total[this.state.labels.length - 1] += 1
                        this.state.data.positif[this.state.labels.length - 1] += 1
                        this.state.data.negatif[this.state.labels.length - 1] += 0
                        this.forceUpdate()
                    } else if (data.val().sentiment === "negatif") {
                        this.state.data.total[this.state.labels.length - 1] += 1
                        this.state.data.positif[this.state.labels.length - 1] += 0
                        this.state.data.negatif[this.state.labels.length - 1] += 1
                        this.forceUpdate()
                    }
                } else {
                    val = data.val().datetime;
                    this.setState({ labels: this.state.labels.concat(data.val().datetime) });
                    if (data.val().sentiment === "positif") {
                        this.setState({ data: { total: this.state.data.total.concat(1), positif: this.state.data.positif.concat(1), negatif: this.state.data.negatif.concat(0) } });
                    } else if (data.val().sentiment === "negatif") {
                        this.setState({ data: { total: this.state.data.total.concat(1), positif: this.state.data.positif.concat(0), negatif: this.state.data.negatif.concat(1) } });
                    } else {
                        this.setState({ data: { total: this.state.data.total.concat(0), positif: this.state.data.positif.concat(0), negatif: this.state.data.negatif.concat(0) } });
                    }
                }
            });
        });
    }
    render() {
        return (
            <div className="dashboard">
                <div className="sidebar">
                    <img class="logo-expand" src="ithb-wh2.png" alt="logo" />
                    <ul>
                        <li><i class="fa fa-area-chart"></i><span>Dashboard</span></li>
                        <li><i class="fa fa-gear"></i><span>Setting</span></li>
                        <li onClick={logout}><i class="fa fa-power-off"></i><span>Logout</span></li>
                    </ul>
                </div>
                <div className="content">
                    <div class="container-fluid">
                        <div class="row h-100">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <Graph
                                            label_props={this.state.labels}
                                            total_props={this.state.data.total}
                                            positif_props={this.state.data.positif}
                                            negatif_props={this.state.data.negatif}
                                        />
                                    </div>
                                </div>
                                <div class="row h-25 summary">
                                    <div class="col-lg-6 positive">
                                        <b>{this.state.data.positif.reduce(function (a, b) { return a + b }, 0)}</b>
                                        <p>tweet positif</p>
                                    </div>
                                    <div class="col-lg-6 negative">
                                        <b>{this.state.data.negatif.reduce(function (a, b) { return a + b }, 0)}</b>
                                        <p>tweet negatif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 feed-container">
                                <div class="feed">{this.state.tweets}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            pass: "",
            isLoggedIn: false
        };
        this.handleEmail = this.handleEmail.bind(this);
        this.handlePass = this.handlePass.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        logout = logout.bind(this);
    }
    handleEmail(event) {
        this.setState({email: event.target.value});
    }
    handlePass(event) {
        this.setState({pass: event.target.value});
    }
    handleSubmit(event) {
        if (this.state.email === "marketing@ithb.com" && this.state.pass === "gogogo") {
            this.setState(state => ({
                isLoggedIn: !state.isLoggedIn
            }));
        } else {
            alert('LOGIN SALAH!');
        }
        event.preventDefault();
    }
    render() {
        let {isLoggedIn} = this.state;
        const renderAuthButton = () =>{
            if(isLoggedIn){
                return <Dashboard />;
            } else{
                return (
                    <div class="container-fluid login-page">
                        <div class="row h-100">
                            <div class="col-4"></div>
                            <div class="col-4">
                                <img class="ithb" src="./ithb-wh2.png" />
                                <div class="card">
                                    <div class="card-body">
                                        <form onSubmit={this.handleSubmit}>
                                            <div class="form-group">
                                                <input class="form-control" type="email" placeholder="example@ithb.xyz" value={this.state.email} onChange={this.handleEmail} />
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" type="password" placeholder="*************" value={this.state.pass} onChange={this.handlePass} />
                                            </div>
                                            <input class="btn login-button" type="submit" value="Submit" />
                                        </form>
                                    </div>
                                </div>
                                <img class="watermark" src="./pixel-audiowide-white.png" width="100" />
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                );
            }
        }
        return (
            <div className="App">
                {renderAuthButton()}
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <App />, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
